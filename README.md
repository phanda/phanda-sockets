#Phanda Sockets
Phanda WebSockets allows you to interact with the PhandaWebSockets of your application in an easy to use way.

Please refer to the documentation on the [Phanda Documentation Site](https://phandaframework.com/docs).