import typescript from 'rollup-plugin-typescript';
import babel from 'rollup-plugin-babel';

export default {
    input: './src/index.ts',
    output: [
        { file: './dist/phanda-sockets.js', format: 'esm' },
        { file: './dist/phanda-sockets.common.js', format: 'cjs' },
        { file: './dist/phanda-sockets.iife.js', format: 'iife', name: 'PhandaSockets' },
    ],
    plugins: [
        typescript(),
        babel({
            exclude: 'node_modules/**',
            presets: ['es2015-rollup', 'stage-2'],
            plugins: ['transform-object-assign']
        })
    ]
}