import Options, {defaultOptions} from "./Contracts/Options";
import ChannelManager from "./Channels/ChannelManager";
import MessageManager from "./Messages/MessageManager";
import ChannelData from "./Contracts/ChannelData";

export default class PhandaSockets {
    protected options?: Options;
    protected retryAttempts: number = 0;
    protected forceClosed = false;

    protected socketId?: string;

    protected connection?: WebSocket;
    protected channelManager?: ChannelManager;
    protected messageManager?: MessageManager;

    constructor(options?: Options) {
        this.configure({...defaultOptions, ...options});
    }

    configure(options: Options) {
        this.options = options;
        this.connect();
    }

    public connect() {
        if (this.connection && this.connection.readyState == WebSocket.OPEN) {
            this.disconnect();
        }

        this.connection = new WebSocket(this.options.url);
        this.messageManager = new MessageManager(this.connection, this);
        this.channelManager = new ChannelManager(this.connection, this);

        this.connection.addEventListener('close', this.onCloseHandler.bind(this));
        this.connection.addEventListener('open', this.onConnectedHandler.bind(this));
        this.onPhandaEvent('connection_established', this.onConnectionEstablished.bind(this));
    }

    public reconnect() {
        this.connection = new WebSocket(this.options.url);
        this.messageManager.setWebSocket(this.connection);
        this.channelManager.setWebSocket(this.connection);
    }

    public disconnect() {
        if (this.connection && this.connection.readyState == WebSocket.OPEN) {
            this.forceClosed = true;
            this.connection.close();
        }

        this.connection = null;
        this.channelManager = null;
        this.messageManager = null;
    }

    public setUrl(url: string) {
        this.options.url = url;
        this.connect();
    }

    public onCloseHandler(event: Event) {
        if (this.options.retryOnFail && this.retryAttempts <= this.options.retryAttempts && !this.forceClosed) {
            this.retryAttempts++;
            this.reconnect();
        }

        this.forceClosed = false;
    }

    public onConnectedHandler(event: Event) {
        this.retryAttempts = 0;
    }

    public getSocketId(): string {
        return this.socketId || '';
    }

    public setSocketId(socketId: string): this {
        this.socketId = socketId;
        return this;
    }

    public subscribe(channel: string) {
        this.channelManager.subscribe(channel, {auth: this.generateAuth(channel)});
    }

    protected generateAuth(channel: string) {
        return this.getSocketId() + ":" + channel;
    }

    public unsubscribe(channel: string) {
        this.channelManager.unsubscribe(channel);
    }

    public getMessageManager(): MessageManager {
        return this.messageManager;
    }

    public getChannelManager(): ChannelManager {
        return this.channelManager;
    }

    public on(eventName: string, callback: (data?: any | null, channel?: string | null) => void) {
        this.messageManager.registerListener(eventName, callback);
    }

    public onPhandaEvent(eventName: string, callback: (data?: any | null, channel?: string | null) => void) {
        return this.on('phanda:' + eventName, callback);
    }

    public onClientEvent(eventName: string, callback: (data?: any | null, channel?: string | null) => void) {
        return this.on('client-' + eventName, callback);
    }

    public send(eventName: string, channelName: string | null = null, data: any[] | Object = []) {
        return this.messageManager.send(eventName, channelName, data);
    }

    public sendPhandaEvent(eventName: string, channelName: string | null = null, data: any[] | Object = []) {
        return this.messageManager.sendPhandaEvent(eventName, channelName, data);
    }

    public sendClientEvent(eventName: string, channelName: string | null = null, data: any[] | Object = []) {
        return this.messageManager.sendClientEvent(eventName, channelName, data);
    }

    protected onConnectionEstablished(data: any|null) {
        this.setSocketId(data.socket_id);
    }

    public setChannelData(data: ChannelData) {
        this.channelManager.setChannelData(data);
    }
}