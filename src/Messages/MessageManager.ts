import PhandaSockets from "../index";

export default class MessageManager {
    protected connection: WebSocket;
    protected manager: PhandaSockets;
    protected listeners: any[];

    constructor(connection: WebSocket, manager: PhandaSockets) {
        this.connection = connection;
        this.manager = manager;
        this.listeners = [];

        this.registerSocketEvents();
    }

    public setWebSocket(socket: WebSocket) {
        this.connection = socket;
        this.registerSocketEvents();
    }

    protected registerSocketEvents() {
        this.connection.addEventListener('open', this.onSocketOpen.bind(this));
        this.connection.addEventListener('close', this.onSocketClose.bind(this));
        this.connection.addEventListener('error', this.onSocketError.bind(this));
        this.connection.addEventListener('message', this.onSocketMessage.bind(this));
    }

    protected onSocketError(event: Event) {
        //
    }

    protected onSocketClose(event: Event) {
        //
    }

    protected onSocketMessage(event: MessageEvent) {
        let data = this.parseMessagePayload(event);

        if (data.event) {
            this.parseEventMessage(data.event, data.data || {}, data.channel || '');
        }
    }

    protected onSocketOpen(event: Event) {
        //
    }

    protected parseMessagePayload(event: Event) {
        if (event instanceof MessageEvent) {
            return JSON.parse(event.data);
        }

        return event;
    }

    protected parseEventMessage(event: string, data: any = {}, channel: string | null = '') {
        this.fireListeners(event, data, channel);
        console.log(event, data, channel);
    }

    protected fireListeners(event: string, data: any = {}, channel: string | null = '') {
        if (this.listeners[event]) {
            this.listeners[event].forEach((callback: (data?: any | null, channel?: string | null) => void) => {
                callback(data, channel);
            })
        }
    }

    protected prepareEventPayload(eventName: string, channelName: string | null = null, data: any[] | Object = []) {
        return JSON.stringify({
            'event': eventName,
            'channel': channelName,
            'data': data
        });
    }

    public send(eventName: string, channelName: string | null = null, data: any[] | Object = []) {
        let payload = this.prepareEventPayload(eventName, channelName, data);
        console.log(payload);
        this.connection.send(payload);
    }

    public sendPhandaEvent(eventName: string, channelName: string | null = null, data: any[] | Object = []) {
        return this.send('phanda:' + eventName, channelName, data);
    }

    public sendClientEvent(eventName: string, channelName: string | null = null, data: any[] | Object = []) {
        return this.send('client-' + eventName, channelName, data);
    }

    public registerListener(eventName: string, callback: (data?: any | null, channel?: string | null) => void) {
        if (!this.listeners[eventName]) {
            this.listeners[eventName] = [];
        }

        this.listeners[eventName].push(callback);
    }

    public removeListener(eventName: string, callback: (data?: any | null, channel?: string | null) => void) {
        this.listeners[eventName].forEach((cb, index) => {
            if (cb == callback) {
                cb = null;
            }
        });
    }

    public removeListeners(eventName: string) {
        this.listeners[eventName] = [];
    }
}