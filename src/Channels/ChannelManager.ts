import PhandaSockets from "../index";
import ChannelData, {defaultChannelData} from "../Contracts/ChannelData";

export default class ChannelManager {

    protected connection: WebSocket;
    protected manager: PhandaSockets;
    protected channelData: ChannelData = defaultChannelData;

    protected channels: any[];

    constructor(connection: WebSocket, manager: PhandaSockets) {
        this.connection = connection;
        this.manager = manager;
        this.channels = [];

        this.manager.onPhandaEvent('subscribed', this.onSubscribe.bind(this));
        this.manager.onPhandaEvent('unsubscribed', this.onUnsubscribe.bind(this));
    }

    public setWebSocket(socket: WebSocket) {
        this.connection = socket;
    }

    public subscribe(channel: string, data?: any | null) {
        if(this.channels.includes(channel)) {
            return;
        }

        this.manager.getMessageManager().sendPhandaEvent('subscribe', channel, {...data, ...{channel_data: this.getChannelData()}});
    }

    public unsubscribe(channel: string) {
        if(!this.channels.includes(channel)) {
            return;
        }

        this.manager.getMessageManager().sendPhandaEvent('unsubscribe', channel);
    }

    protected onSubscribe(data: any|null, channel: string) {
        this.channels.push(channel);
    }

    protected onUnsubscribe(data: any|null, channel) {
        this.channels = this.channels.filter(c => c !== channel);
    }

    protected getChannelData() {
        return this.channelData;
    }

    public setChannelData(data: ChannelData) {
        this.channelData = data;
    }

}