export default interface Options {
    url: string,
    retryOnFail?: boolean,
    retryAttempts?: number
}

export const defaultOptions: Options = {
    url: 'ws://127.0.0.1:7001/app/default',
    retryOnFail: true,
    retryAttempts: 3
};