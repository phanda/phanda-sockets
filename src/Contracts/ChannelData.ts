export default interface ChannelData {
    user_id: string | number,
    user_info: any
}

export const defaultChannelData = {
    user_id: 0,
    user_info: {
        user_name: "Guest"
    }
};