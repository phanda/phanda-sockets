export default interface Options {
    url: string;
    retryOnFail?: boolean;
    retryAttempts?: number;
}
export declare const defaultOptions: Options;
