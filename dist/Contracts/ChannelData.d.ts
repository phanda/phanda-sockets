export default interface ChannelData {
    user_id: string | number;
    user_info: any;
}
export declare const defaultChannelData: {
    user_id: number;
    user_info: {
        user_name: string;
    };
};
