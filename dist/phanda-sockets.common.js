'use strict';

var defaultOptions = {
    url: 'ws://127.0.0.1:7001/app/default',
    retryOnFail: true,
    retryAttempts: 3
};

var defaultChannelData = {
    user_id: 0,
    user_info: {
        user_name: "Guest"
    }
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var ChannelManager = function () {
    function ChannelManager(connection, manager) {
        classCallCheck(this, ChannelManager);

        this.channelData = defaultChannelData;
        this.connection = connection;
        this.manager = manager;
        this.channels = [];
        this.manager.onPhandaEvent('subscribed', this.onSubscribe.bind(this));
        this.manager.onPhandaEvent('unsubscribed', this.onUnsubscribe.bind(this));
    }

    createClass(ChannelManager, [{
        key: 'setWebSocket',
        value: function setWebSocket(socket) {
            this.connection = socket;
        }
    }, {
        key: 'subscribe',
        value: function subscribe(channel, data) {
            if (this.channels.includes(channel)) {
                return;
            }
            this.manager.getMessageManager().sendPhandaEvent('subscribe', channel, _extends({}, data, { channel_data: this.getChannelData() }));
        }
    }, {
        key: 'unsubscribe',
        value: function unsubscribe(channel) {
            if (!this.channels.includes(channel)) {
                return;
            }
            this.manager.getMessageManager().sendPhandaEvent('unsubscribe', channel);
        }
    }, {
        key: 'onSubscribe',
        value: function onSubscribe(data, channel) {
            this.channels.push(channel);
        }
    }, {
        key: 'onUnsubscribe',
        value: function onUnsubscribe(data, channel) {
            this.channels = this.channels.filter(function (c) {
                return c !== channel;
            });
        }
    }, {
        key: 'getChannelData',
        value: function getChannelData() {
            return this.channelData;
        }
    }, {
        key: 'setChannelData',
        value: function setChannelData(data) {
            this.channelData = data;
        }
    }]);
    return ChannelManager;
}();

var MessageManager = function () {
    function MessageManager(connection, manager) {
        classCallCheck(this, MessageManager);

        this.connection = connection;
        this.manager = manager;
        this.listeners = [];
        this.registerSocketEvents();
    }

    createClass(MessageManager, [{
        key: 'setWebSocket',
        value: function setWebSocket(socket) {
            this.connection = socket;
            this.registerSocketEvents();
        }
    }, {
        key: 'registerSocketEvents',
        value: function registerSocketEvents() {
            this.connection.addEventListener('open', this.onSocketOpen.bind(this));
            this.connection.addEventListener('close', this.onSocketClose.bind(this));
            this.connection.addEventListener('error', this.onSocketError.bind(this));
            this.connection.addEventListener('message', this.onSocketMessage.bind(this));
        }
    }, {
        key: 'onSocketError',
        value: function onSocketError(event) {
            //
        }
    }, {
        key: 'onSocketClose',
        value: function onSocketClose(event) {
            //
        }
    }, {
        key: 'onSocketMessage',
        value: function onSocketMessage(event) {
            var data = this.parseMessagePayload(event);
            if (data.event) {
                this.parseEventMessage(data.event, data.data || {}, data.channel || '');
            }
        }
    }, {
        key: 'onSocketOpen',
        value: function onSocketOpen(event) {
            //
        }
    }, {
        key: 'parseMessagePayload',
        value: function parseMessagePayload(event) {
            if (event instanceof MessageEvent) {
                return JSON.parse(event.data);
            }
            return event;
        }
    }, {
        key: 'parseEventMessage',
        value: function parseEventMessage(event) {
            var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
            var channel = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

            this.fireListeners(event, data, channel);
            console.log(event, data, channel);
        }
    }, {
        key: 'fireListeners',
        value: function fireListeners(event) {
            var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
            var channel = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

            if (this.listeners[event]) {
                this.listeners[event].forEach(function (callback) {
                    callback(data, channel);
                });
            }
        }
    }, {
        key: 'prepareEventPayload',
        value: function prepareEventPayload(eventName) {
            var channelName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

            return JSON.stringify({
                'event': eventName,
                'channel': channelName,
                'data': data
            });
        }
    }, {
        key: 'send',
        value: function send(eventName) {
            var channelName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

            var payload = this.prepareEventPayload(eventName, channelName, data);
            console.log(payload);
            this.connection.send(payload);
        }
    }, {
        key: 'sendPhandaEvent',
        value: function sendPhandaEvent(eventName) {
            var channelName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

            return this.send('phanda:' + eventName, channelName, data);
        }
    }, {
        key: 'sendClientEvent',
        value: function sendClientEvent(eventName) {
            var channelName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

            return this.send('client-' + eventName, channelName, data);
        }
    }, {
        key: 'registerListener',
        value: function registerListener(eventName, callback) {
            if (!this.listeners[eventName]) {
                this.listeners[eventName] = [];
            }
            this.listeners[eventName].push(callback);
        }
    }, {
        key: 'removeListener',
        value: function removeListener(eventName, callback) {
            this.listeners[eventName].forEach(function (cb, index) {
                if (cb == callback) {
                    cb = null;
                }
            });
        }
    }, {
        key: 'removeListeners',
        value: function removeListeners(eventName) {
            this.listeners[eventName] = [];
        }
    }]);
    return MessageManager;
}();

var PhandaSockets = function () {
    function PhandaSockets(options) {
        classCallCheck(this, PhandaSockets);

        this.retryAttempts = 0;
        this.forceClosed = false;
        this.configure(_extends({}, defaultOptions, options));
    }

    createClass(PhandaSockets, [{
        key: "configure",
        value: function configure(options) {
            this.options = options;
            this.connect();
        }
    }, {
        key: "connect",
        value: function connect() {
            if (this.connection && this.connection.readyState == WebSocket.OPEN) {
                this.disconnect();
            }
            this.connection = new WebSocket(this.options.url);
            this.messageManager = new MessageManager(this.connection, this);
            this.channelManager = new ChannelManager(this.connection, this);
            this.connection.addEventListener('close', this.onCloseHandler.bind(this));
            this.connection.addEventListener('open', this.onConnectedHandler.bind(this));
            this.onPhandaEvent('connection_established', this.onConnectionEstablished.bind(this));
        }
    }, {
        key: "reconnect",
        value: function reconnect() {
            this.connection = new WebSocket(this.options.url);
            this.messageManager.setWebSocket(this.connection);
            this.channelManager.setWebSocket(this.connection);
        }
    }, {
        key: "disconnect",
        value: function disconnect() {
            if (this.connection && this.connection.readyState == WebSocket.OPEN) {
                this.forceClosed = true;
                this.connection.close();
            }
            this.connection = null;
            this.channelManager = null;
            this.messageManager = null;
        }
    }, {
        key: "setUrl",
        value: function setUrl(url) {
            this.options.url = url;
            this.connect();
        }
    }, {
        key: "onCloseHandler",
        value: function onCloseHandler(event) {
            if (this.options.retryOnFail && this.retryAttempts <= this.options.retryAttempts && !this.forceClosed) {
                this.retryAttempts++;
                this.reconnect();
            }
            this.forceClosed = false;
        }
    }, {
        key: "onConnectedHandler",
        value: function onConnectedHandler(event) {
            this.retryAttempts = 0;
        }
    }, {
        key: "getSocketId",
        value: function getSocketId() {
            return this.socketId || '';
        }
    }, {
        key: "setSocketId",
        value: function setSocketId(socketId) {
            this.socketId = socketId;
            return this;
        }
    }, {
        key: "subscribe",
        value: function subscribe(channel) {
            this.channelManager.subscribe(channel, { auth: this.generateAuth(channel) });
        }
    }, {
        key: "generateAuth",
        value: function generateAuth(channel) {
            return this.getSocketId() + ":" + channel;
        }
    }, {
        key: "unsubscribe",
        value: function unsubscribe(channel) {
            this.channelManager.unsubscribe(channel);
        }
    }, {
        key: "getMessageManager",
        value: function getMessageManager() {
            return this.messageManager;
        }
    }, {
        key: "getChannelManager",
        value: function getChannelManager() {
            return this.channelManager;
        }
    }, {
        key: "on",
        value: function on(eventName, callback) {
            this.messageManager.registerListener(eventName, callback);
        }
    }, {
        key: "onPhandaEvent",
        value: function onPhandaEvent(eventName, callback) {
            return this.on('phanda:' + eventName, callback);
        }
    }, {
        key: "onClientEvent",
        value: function onClientEvent(eventName, callback) {
            return this.on('client-' + eventName, callback);
        }
    }, {
        key: "send",
        value: function send(eventName) {
            var channelName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

            return this.messageManager.send(eventName, channelName, data);
        }
    }, {
        key: "sendPhandaEvent",
        value: function sendPhandaEvent(eventName) {
            var channelName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

            return this.messageManager.sendPhandaEvent(eventName, channelName, data);
        }
    }, {
        key: "sendClientEvent",
        value: function sendClientEvent(eventName) {
            var channelName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

            return this.messageManager.sendClientEvent(eventName, channelName, data);
        }
    }, {
        key: "onConnectionEstablished",
        value: function onConnectionEstablished(data) {
            this.setSocketId(data.socket_id);
        }
    }, {
        key: "setChannelData",
        value: function setChannelData(data) {
            this.channelManager.setChannelData(data);
        }
    }]);
    return PhandaSockets;
}();

module.exports = PhandaSockets;
