import PhandaSockets from "../index";
export default class MessageManager {
    protected connection: WebSocket;
    protected manager: PhandaSockets;
    protected listeners: any[];
    constructor(connection: WebSocket, manager: PhandaSockets);
    setWebSocket(socket: WebSocket): void;
    protected registerSocketEvents(): void;
    protected onSocketError(event: Event): void;
    protected onSocketClose(event: Event): void;
    protected onSocketMessage(event: MessageEvent): void;
    protected onSocketOpen(event: Event): void;
    protected parseMessagePayload(event: Event): any;
    protected parseEventMessage(event: string, data?: any, channel?: string | null): void;
    protected fireListeners(event: string, data?: any, channel?: string | null): void;
    protected prepareEventPayload(eventName: string, channelName?: string | null, data?: any[] | Object): string;
    send(eventName: string, channelName?: string | null, data?: any[] | Object): void;
    sendPhandaEvent(eventName: string, channelName?: string | null, data?: any[] | Object): void;
    sendClientEvent(eventName: string, channelName?: string | null, data?: any[] | Object): void;
    registerListener(eventName: string, callback: (data?: any | null, channel?: string | null) => void): void;
    removeListener(eventName: string, callback: (data?: any | null, channel?: string | null) => void): void;
    removeListeners(eventName: string): void;
}
