import PhandaSockets from "../index";
import ChannelData from "../Contracts/ChannelData";
export default class ChannelManager {
    protected connection: WebSocket;
    protected manager: PhandaSockets;
    protected channelData: ChannelData;
    protected channels: any[];
    constructor(connection: WebSocket, manager: PhandaSockets);
    setWebSocket(socket: WebSocket): void;
    subscribe(channel: string, data?: any | null): void;
    unsubscribe(channel: string): void;
    protected onSubscribe(data: any | null, channel: string): void;
    protected onUnsubscribe(data: any | null, channel: any): void;
    protected getChannelData(): ChannelData;
    setChannelData(data: ChannelData): void;
}
